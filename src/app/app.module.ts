import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFireModule} from "angularfire2";
import {AngularFireDatabaseModule} from "angularfire2/database";


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {AddPage} from "../pages/add/add";
import {Camera} from "@ionic-native/camera";

const firebaseConfig  = {
  apiKey: "AIzaSyB--GWSkwxoEFaVVOYXOM-j8d4Wlu6gpa8",
  authDomain: "rest-ordering.firebaseapp.com",
  databaseURL: "https://rest-ordering.firebaseio.com",
  projectId: "rest-ordering",
  storageBucket: "rest-ordering.appspot.com",
  messagingSenderId: "893437556963"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPage
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
