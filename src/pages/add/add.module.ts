import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPage } from './add';

@NgModule({
  imports: [
    IonicPageModule.forChild(AddPage),
  ],
})
export class AddPageModule {}
