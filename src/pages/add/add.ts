import { Component } from '@angular/core';
import {ActionSheetController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {Camera} from "@ionic-native/camera";
import firebase from 'firebase';

/**
 * Generated class for the AddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {

  food : any = {
    id : '',
    img_url : 'assets/imgs/blank_img.png',
    name : '',
    description : '',
    price : 0,
    show : true
  };
  picdata : any;
  private mypicref : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private afDB : AngularFireDatabase,
              private loadingCtrl : LoadingController,
              private actionSheetCtrl : ActionSheetController,
              private camera : Camera) {
  }

  ionViewDidLoad() {
    this.mypicref = firebase.storage().ref('/pictures/');
  }

  addFood() {
    if(!this.food.name || !this.food.price || !this.food.description || this.food.img_url == 'assets/imgs/blank_img.png'){
      alert('Please fill all info of food'); return;
    }
    let key = this.afDB.list('/foods').push(this.food).key;
    this.food.id = key;
    this.afDB.object('/foods/' + key).update(this.food);
    this.navCtrl.pop();
  }

  takeNewPic(from : number){
    console.log("Take new photo");
    this.camera.getPicture({
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : from,
      encodingType : this.camera.EncodingType.PNG,
      allowEdit : true,
      targetWidth : 250,
      targetHeight : 200,
      saveToPhotoAlbum : false
    }).then(imagedata =>{
      this.picdata = imagedata;
      this.upload();
    }).catch(err=>console.log(err));
  }

  upload(){

    let loading = this.loadingCtrl.create({
      content: 'uploading...'
    });

    loading.present();
    this.mypicref.child(this.uid()).child('pic.png')
      .putString(this.picdata,'base64',{contentType : 'image/png'})
      .then(savepic =>{        // this.regUser.profileImg = savepic.downloadURL;
        // console.log(savepic.downloadURL);
        savepic.ref.getDownloadURL().then((url)=>{
          this.food.img_url = url;
          loading.dismiss();
          console.log(url);
        })
      })
  }
  uid() {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Take your photo',
      buttons: [
        {
          text: 'From Camera',
          handler: () => {
            this.takeNewPic(1);
          }
        },
        {
          text: 'From Gallery',
          handler: () => {
            this.takeNewPic(0);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
