import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {AddPage} from "../add/add";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  foods : any = [];
  objectSubscription : any;
  count = 0;
  constructor(public navCtrl: NavController, private afDB : AngularFireDatabase) {
  }
  ionViewWillLeave(){
    this.objectSubscription.unsubscribe();
  }

  ionViewWillEnter(){
    this.objectSubscription = this.afDB.object('/foods').valueChanges()
      .subscribe(snapshots=>{
        console.log(snapshots);
        if(snapshots){
          let results = Object.keys(snapshots);
          this.foods = [];
          this.count = 0;
          results.forEach((key,index)=> {
            let food = snapshots[key];
            if(food.show){
              this.foods.push(food);
              this.count++;
            }
          });
        }
      })
  }
  addProduct() {
    this.navCtrl.push(AddPage);
  }
  delete(id){
    this.afDB.object('/foods/' + id + '/show').set(false);
  }

}
